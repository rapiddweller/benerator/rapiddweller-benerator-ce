## Preface

This document is supposed to become a complete summary of everything you need of benerator usage, use it efficiently and extend it as you need. This reference is under construction and will update from time to time. Feel free to contribute your ideas in our repo at [https://gitlab.com/rapiddweller/benerator/rapiddweller-benerator-ce/](https://gitlab.com/rapiddweller/benerator/rapiddweller-benerator-ce/-/issues).

If problems remain unsolved after reading this book, do not hesitate to contact us for help. rapiddweller-benerator-ce is and remains open source and is provided for free.

If you are interested in additional support and our premium features, we encourage you to check the website [https://www.benerator.de](https://www.benerator.de/). We offer additional services to make your data generation project a success and provide detailed use cases to ease your start into more complex scenarios.

Since you can do quite a lot of different things with benerator but surely are interested in just a part of it, here's some guidance:

**'Introduction to benerator'**, introduces you to goals and features of benerator and advises you how to install a binary distribution and how to get the sources and set up an Eclipse project for using, debugging and customizing benerator.

**'Data Generation Concepts'**, **'Descriptor File Format'** and **'Advanced Topics'** then provide you with a structured and complete introduction into the benerator descriptor file setup.

Benerator supports a multitude of service provider interfaces (SPIs). It comes along with some implementations for specific business domains (**'Domains'**) and general purpose classes in **'Component Reference'**.

Finally you are instructed how to write custom SPI implementations in **'Extending benerator'**.

# Summary

* [Introduction to benerator](introduction_to_benerator.md)
* [Installation](installation.md)
* [The Benerator Project Wizard](the_benerator_project_wizard.md)
* [Quick tour through the descriptor file format](quick_tour_through_the_descriptor_file_format.md)
* [Data Generation Concepts](data_generation_concepts.md)
* [Regular Expression Support](regular_expression_support.md)
* [Processing and creating CSV Files](processing_and_creating_csv_files.md)
* [Using Relational Databases](using_relational_databases.md)
* [Using mongoDB](using_mongodb.md)
* [Generating XML Files](generating_xml_files.md)
* [Advanced Topics](advanced_topics.md)
* [Generating Unique Data](generating_unique_data.md)
* [Scripting](scripting.md)
* [DatabeneScript](databenescript.md)
* [Command Line Tools](command_line_tools.md)
* [Domains](domains.md)
* [Component Reference](component_reference.md)
* [Using DB Sanity](using_db_sanity.md)
* [Maven Benerator Plugin](maven_benerator_plugin.md)
* [Extending benerator](extending_benerator.md)
* [Using Benerator as Load Generator](using_benerator_as_load_generator.md)
* [Troubleshooting](troubleshooting.md)
* [Monitoring Benerator](monitoring_benerator.md)
* [Benerator Performance Tuning](benerator_performance_tuning.md)
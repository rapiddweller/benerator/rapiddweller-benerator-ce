/*
 * (c) Copyright 2006-2020 by rapiddweller GmbH & Volker Bergmann. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is permitted under the terms of the
 * GNU General Public License.
 *
 * For redistributing this software or a derivative work under a license other
 * than the GPL-compatible Free Software License as defined by the Free
 * Software Foundation or approved by OSI, you must first obtain a commercial
 * license to this software product from rapiddweller GmbH & Volker Bergmann.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * WITHOUT A WARRANTY OF ANY KIND. ALL EXPRESS OR IMPLIED CONDITIONS,
 * REPRESENTATIONS AND WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE
 * HEREBY EXCLUDED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.rapiddweller.benerator.engine;

import com.rapiddweller.benerator.test.BeneratorIntegrationTest;
import com.rapiddweller.common.FileUtil;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * Integration test for Benerator's Demo Files.<br/><br/>
 * <p>
 * Created at 30.12.2020
 *
 * @author Alexander Kell
 * @since 1.1.0
 */
public class DemoIntegrationTest extends BeneratorIntegrationTest {
    String ROOT = "src/demo/resources/";

    private void parseAndExecute() throws IOException {
        for (File file : Objects.requireNonNull(new File(ROOT, context.getContextUri()).listFiles())) {
            String filename = file.getPath();
            if (FileUtil.isXMLFile(filename)) {
                System.out.println(filename);
                parseAndExecuteFile(filename);
            }
        }
    }

    @Test
    public void DemoFiles() throws IOException {
        context.setContextUri("/demo/file");
        parseAndExecute();
    }

    @Test
    public void DemoDb() throws IOException {
        context.setContextUri("/demo/db");
        parseAndExecute();
    }


    @Test
    public void DemoScript() throws IOException {
        context.setContextUri("/demo/script");
        parseAndExecute();
    }


}
